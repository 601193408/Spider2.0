/**   
 * Copyright © 2018 LeadingSoft. All rights reserved.
 * 
 * @Title package-info.java 
 * @Prject Spider2.0
 * @Package com.leadingsoft.core 
 * @Description 新版爬虫，使用Java调用浏览器引擎
 * @author gongym   
 * @date 2018年6月4日 上午11:54:30 
 * @version V1.0   
 */
/**
 * @ClassName package-info
 * @Description 核心库
 * @author gongym
 * @date 2018年6月4日 上午11:54:30
 */
package com.leadingsoft.core;
