package com.leadingsoft.controller.parse;

import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public interface GetDocumentData {
	Object elementToObject(Element element);
	List<Object> elementsToObjects(Elements elements);
}
