package com.leadingsoft.controller.browser;

/**
 * @ClassName BrowserInstance
 * @Description 浏览器实例接口，可以实现自己的浏览器实例
 * @author gongym
 * @date 2018年6月10日 下午4:27:43
 */
public interface BrowserInstance {
	void createBrowserAndCrawler(Long startIndex, Long stopIndex);
}
