package com.leadingsoft.controller.browser.impl;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.ConnectionSource;
import org.beetl.sql.core.ConnectionSourceHelper;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.SQLLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.handler.CefLoadHandlerAdapter;

import com.leadingsoft.common.CheckUrl;
import com.leadingsoft.common.TaskQuene;
import com.leadingsoft.controller.browser.BrowserInstance;
import com.leadingsoft.controller.parse.DownloadHtmlDocument;
import com.leadingsoft.controller.parse.ParsingHtmlDocument;
import com.leadingsoft.core.CommonConfig;
import com.leadingsoft.ui.BrowserFrame;

/**
 * @ClassName UrlBrowserInstance
 * @Description Url浏览器实例<br>
 *              进行数据抓取<br>
 *              下一页是通过修改浏览器地址进行获取<br>
 *              示例网站：携程酒店信息
 * @author gongym
 * @date 2018年6月7日 下午5:09:30
 */
public class UrlBrowserInstance implements BrowserInstance {
	private CefClient nowClient;
	private Boolean nowOsrEnabled;
	private Boolean nowTransparentPaintingEnabled;
	// 配置对象
	private Configuration config;

	public UrlBrowserInstance(CefClient client, boolean osrEnabled, boolean transparentPaintingEnabled) {
		this.nowClient = client;
		this.nowOsrEnabled = osrEnabled;
		this.nowTransparentPaintingEnabled = transparentPaintingEnabled;
		this.config = CommonConfig.getInstance();
	}
	/**
	 * @Title createBrowserAndCrawler
	 * @Description 创建浏览器实例
	 * @param startIndex
	 * @param stopIndex
	 * @return void
	 */
	@Override
	public void createBrowserAndCrawler(Long startIndex, Long stopIndex) {
		// 给一个起始页创建浏览器对象
		CefBrowser browser = nowClient.createBrowser("http://www.baidu.com", nowOsrEnabled,
				nowTransparentPaintingEnabled);
		// 数据库连接对象
		String datasourceDriver = config.getString("datasource.driver");
		String datasourceUrl = config.getString("datasource.url");
		String datasourceUsername = config.getString("datasource.username");
		String datasourcePassword = config.getString("datasource.password");
		ConnectionSource source = ConnectionSourceHelper.getSimple(datasourceDriver, datasourceUrl, datasourceUsername,
				datasourcePassword);
		DBStyle mysql = new MySqlStyle();
		SQLLoader loader = new ClasspathLoader("/");
		UnderlinedNameConversion nc = new UnderlinedNameConversion();
		SQLManager sqlManager = new SQLManager(mysql, loader, source, nc, new Interceptor[] { new DebugInterceptor() });
		Integer isDownload = config.getInt("is_download");
		// 获取配置文件对象
		nowClient.addLoadHandler(new CefLoadHandlerAdapter() {
			@Override
			public void onLoadEnd(CefBrowser browser, CefFrame frame, int what) {
				String url = browser.getURL();
				if (CheckUrl.isListUrl(url)) {
					if (isDownload.equals(1)) {
						DownloadHtmlDocument htmlSource = new DownloadHtmlDocument(url);
						browser.getSource(htmlSource);
						String nextUrl = TaskQuene.getCtripHotelListUrl(url, startIndex, stopIndex);
						if (StringUtils.isEmpty(nextUrl)) {
							TaskQuene.taskUrl.add(nextUrl.toString());
						} else {
							// 当前任务结束
							// 关闭浏览器
							browser.close();
						}
					} else {
						String listSelector = "hotel_new_list";
						new ParsingHtmlDocument(url, listSelector, sqlManager);
					}
				} else {
					String startUrl = TaskQuene.getCtripHotelListUrl(null, startIndex, stopIndex);
					TaskQuene.taskUrl.add(startUrl);
				}
			}
		});
		final BrowserFrame frame = new BrowserFrame(nowClient, browser);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
				CefApp.getInstance().dispose();
			}
		});
		frame.setSize(1000, 500);
		frame.setVisible(true);
		// 开始抓取数据
		crawlerData(browser);
	}
	/**
	 * @Title crawlerData
	 * @Description 循环列表抓取数据
	 * @param startIndex
	 * @param stopIndex
	 * @return void
	 */
	private void crawlerData(CefBrowser browser) {
		while (true) {// 循环调用，如果队列中有任务就会加载页面
			String ctripHotelListUrl = TaskQuene.taskUrl.poll();
			if (null != ctripHotelListUrl) {
				// 加载页面
				browser.loadURL(ctripHotelListUrl);
			}
		}
	}
}
