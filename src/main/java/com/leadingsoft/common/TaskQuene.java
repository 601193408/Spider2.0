package com.leadingsoft.common;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.lang3.StringUtils;

import com.leadingsoft.common.model.Hotel;

/**
 * @ClassName TaskQuene
 * @Description 任务队列
 * @author gongym
 * @date 2018年6月6日 下午2:54:31
 */
public class TaskQuene {
	public static LinkedBlockingQueue<String> taskUrl = new LinkedBlockingQueue<String>();
	// public static ConcurrentLinkedQueue<String> taskUrl = new ConcurrentLinkedQueue<String>();

	/**
	 * @Title getCtripHotelListUrl
	 * @Description 携程酒店北京地区的酒店列表URL
	 * @param url 当前URL
	 * @param startIndex 起始页数
	 * @param stopIndex 结束页数
	 * @return
	 * @return String
	 */
	public static String getCtripHotelListUrl(String url, Long startIndex, Long stopIndex) {
		if (null != url) {
			String index = StringUtils.remove(url, Constant.CTRIPHOTELLISTUTLTEMP);
			Long nextIndex = Long.parseLong(index) + 1;
			if (nextIndex <= stopIndex) {
				StringBuilder nextUrl = new StringBuilder(Constant.CTRIPHOTELLISTUTLTEMP);
				nextUrl.append(nextIndex);
				return nextUrl.toString();
			} else {
				return null;
			}
		} else {
			StringBuilder startUrl = new StringBuilder(Constant.CTRIPHOTELLISTUTLTEMP);
			startUrl.append(startIndex);
			return startUrl.toString();
		}
	}
	/**
	 * @Title: getCtripHotelCommentListUrl
	 * @Description: 携程酒店北京地区的酒店评论列表URL
	 * @param url 当前页URL
	 * @param hotelList 酒店列表
	 * @param hotelIdList 酒店ID列表
	 * @return
	 * @return: String
	 */
	public static String getCtripHotelCommentListUrl(String url, List<Hotel> hotelList, List<String> hotelIdList) {
		if (null != url) {
			String[] urls = url.split("_");
			String hotelId = StringUtils.remove(urls[0], Constant.CTRIPHOTELCOMMENTURLTEMP);
			Integer pageIndex = Integer.parseInt(urls[1].replace("t0.html", "").replace("p", ""));
			if (hotelIdList.contains(hotelId)) {
				Integer hotelIndex = hotelIdList.indexOf(hotelId);
				Hotel hotel = hotelList.get(hotelIndex);
				Integer hotelJudgement = hotel.getHotelJudgement();
				Integer totalPages = hotelJudgement / Constant.EVERYPAGECOMMENTNUM + 1;
				if (pageIndex < totalPages) {// 当前酒店的评论信息还没有抓取完毕
					// 拼接URL
					StringBuilder nextUrl = new StringBuilder(Constant.CTRIPHOTELCOMMENTURLTEMP);
					nextUrl.append(hotelId).append("_p").append(pageIndex + 1).append("t0.html");
					return nextUrl.toString();
				} else if (pageIndex.equals(totalPages)) {// 当前酒店的评论信息已经抓取完毕
					// 拼接下一个酒店的
					Integer nextIndex = hotelIndex + 1;
					if (nextIndex < hotelList.size()) {// 判断是否有下一个酒店信息
						Hotel nextHotel = hotelList.get(nextIndex);
						StringBuilder nextUrl = new StringBuilder(Constant.CTRIPHOTELCOMMENTURLTEMP);
						nextUrl.append(nextHotel.getHotelId()).append("_p1t0.html");
						return nextUrl.toString();
					} else {// 没有下一个酒店信息，证明抓取完毕
						return null;
					}
				} else {// 异常情况
					return null;
				}
			} else {
				return null;
			}
		} else {
			Hotel hotel = hotelList.get(0);
			String hotelId = hotel.getHotelId();
			StringBuilder startUrl = new StringBuilder(Constant.CTRIPHOTELCOMMENTURLTEMP);
			startUrl.append(hotelId).append("_p1t0.html");
			return startUrl.toString();
		}
	}
}
