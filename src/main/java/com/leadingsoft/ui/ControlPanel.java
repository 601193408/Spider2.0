package com.leadingsoft.ui;

import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cef.OS;
import org.cef.browser.CefBrowser;

public class ControlPanel extends JPanel {
	private static final long serialVersionUID = -1729022602136743541L;
	private final JButton backButton;
	private final JButton forwardButton;
	private final JButton reloadButton;
	private final JTextField addressField;
	private final JLabel zoomLabel;
	private double zoomLevel = 0;
	private final CefBrowser browser;

	public ControlPanel(CefBrowser nowBrowser) {
		browser = nowBrowser;
		setEnabled(browser != null);
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(Box.createHorizontalStrut(5));
		add(Box.createHorizontalStrut(5));
		backButton = new JButton("后退");
		backButton.setAlignmentX(LEFT_ALIGNMENT);
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browser.goBack();
			}
		});
		add(backButton);
		add(Box.createHorizontalStrut(5));
		forwardButton = new JButton("向前");
		forwardButton.setAlignmentX(LEFT_ALIGNMENT);
		forwardButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browser.goForward();
			}
		});
		add(forwardButton);
		add(Box.createHorizontalStrut(5));
		reloadButton = new JButton("重载");
		reloadButton.setAlignmentX(LEFT_ALIGNMENT);
		reloadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (reloadButton.getText().equalsIgnoreCase("reload")) {
					int mask = OS.isMacintosh() ? ActionEvent.META_MASK : ActionEvent.CTRL_MASK;
					if ((e.getModifiers() & mask) != 0) {
						System.out.println("Reloading - ignoring cached values");
						browser.reloadIgnoreCache();
					} else {
						System.out.println("Reloading - using cached values");
						browser.reload();
					}
				} else {
					browser.stopLoad();
				}
			}
		});
		add(reloadButton);
		add(Box.createHorizontalStrut(5));
		JLabel addressLabel = new JLabel("地址:");
		addressLabel.setAlignmentX(LEFT_ALIGNMENT);
		add(addressLabel);
		add(Box.createHorizontalStrut(5));
		addressField = new JTextField(100);
		addressField.setAlignmentX(LEFT_ALIGNMENT);
		addressField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browser.loadURL(getAddress());
			}
		});
		addressField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
				addressField.requestFocus();
			}
		});
		add(addressField);
		add(Box.createHorizontalStrut(5));
		JButton goButton = new JButton("访问");
		goButton.setAlignmentX(LEFT_ALIGNMENT);
		goButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browser.loadURL(getAddress());
			}
		});
		add(goButton);
		add(Box.createHorizontalStrut(5));
		JButton minusButton = new JButton("-");
		minusButton.setAlignmentX(CENTER_ALIGNMENT);
		minusButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browser.setZoomLevel(--zoomLevel);
				zoomLabel.setText(new Double(zoomLevel).toString());
			}
		});
		add(minusButton);
		zoomLabel = new JLabel("0.0");
		add(zoomLabel);
		JButton plusButton = new JButton("+");
		plusButton.setAlignmentX(CENTER_ALIGNMENT);
		plusButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browser.setZoomLevel(++zoomLevel);
				zoomLabel.setText(new Double(zoomLevel).toString());
			}
		});
		add(plusButton);
	}
	public void update(CefBrowser nowBrowser, boolean isLoading, boolean canGoBack, boolean canGoForward) {
		if (browser == nowBrowser) {
			backButton.setEnabled(canGoBack);
			forwardButton.setEnabled(canGoForward);
			reloadButton.setText(isLoading ? "关于" : "重载");
		}
	}
	public String getAddress() {
		String address = addressField.getText();
		try {
			address = address.replaceAll(" ", "%20");
			URI test = new URI(address);
			if (test.getScheme() != null)
				return address;
			if (test.getHost() != null && test.getPath() != null)
				return address;
			String specific = test.getSchemeSpecificPart();
			if (specific.indexOf('.') == -1)
				throw new URISyntaxException(specific, "No dot inside domain");
		} catch (URISyntaxException e1) {
			address = "search://" + address;
		}
		return address;
	}
	public void setAddress(CefBrowser nowBrowser, String address) {
		if (browser == nowBrowser)
			addressField.setText(address);
	}
}
