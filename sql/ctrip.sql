/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : ctrip

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2018-06-08 16:00:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hotel
-- ----------------------------
DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel` (
  `hotel_id` varchar(15) NOT NULL,
  `hotel_name` varchar(100) DEFAULT NULL,
  `hotel_strategymedal` int(1) DEFAULT NULL COMMENT '1:是；0：不是（携程战略合作酒店，拥有优质服务、优良品质及优惠房价，供携程会员专享预订）',
  `hotel_address` varchar(300) DEFAULT NULL,
  `hotel_price` int(11) DEFAULT NULL,
  `hotel_special_label` varchar(100) DEFAULT NULL,
  `hotel_facility` varchar(100) DEFAULT NULL,
  `hotel_level` varchar(50) DEFAULT NULL,
  `hotel_value` float DEFAULT NULL,
  `hotel_total_judgement_score` int(2) DEFAULT NULL,
  `hotel_judgement` int(11) DEFAULT NULL,
  `hotel_recommend` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`hotel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hotel
-- ----------------------------
